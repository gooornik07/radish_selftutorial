<<<<<<< HEAD
1)Instalacja pipa z argumentem proxy: pip install .whl --proxy=nazwa:port
2)Odpalenie na windowsie z argumentem basedir (problem pwd): radish -b tests/radish tests/calculator.feature
3)Generacja raportu w formacie JSON: radish -b tests/radish tests/calculator.feature --cucumber-json tests/result2.json 
4)Generacja raportu w formacie XML (JUnit): radish -b tests/radish tests/calculator.feature --junit-xml tests/result.xml (?)
5)Generacja coverage: radish -b tests/radish tests/calculator.feature --with-coverage


cucumber-html-reporter, Windows:

- pobrac node-js, kt�ry zawiera npm
- instalacja cucumber-html-reporter z ustawieniem proxy:
	npm config set strict-ssl false
	npm --proxy ... --without-ssl --insecure -g install -g cucumber-html-reporter --save --only=dev


Na Centosie:

- yum install nodejs
- yum install npm 
- nodejs --version
- npm --version


Konfig srodowiska - PyCharm:

1) Otwarcie linii komend: 
ALT + F12 lub ALT + ALT + klik

2) Instalacja wtyczki Gherkin:
CTRL + ALT + S -> Plugins -> Install Jetbrains Plugins -> HTTP Proxy Settings ->
-> Manual proxy configuration -> nazwa, port ->
-> wyszukac: Gherkin -> zainstalowac druga wtyczke -> restart Pycharma

Na krokach testowych bedzie jeszcze z�lty warning - aby go usunac:
ALT + ENTER -> undefined step inspection -> odznaczyc

Niestety tylko na wersji professional mozna jeszcze przechodzic miedzy krokami, autouzupelnienie, konfiguracja uruchomienia itd:
https://blog.jetbrains.com/pycharm/2014/09/feature-spotlight-behavior-driven-development-in-pycharm/
https://www.jetbrains.com/help/pycharm/creating-step-definition.html

3) Zmiana taby <--> spacje: Edit -> Convert indents -> Tabs/Spaces
Przed @given/@when/@then dwie linie przerwy.
Jak dla mnie lepiej z tabami, bo wtedy mozna lokalnie ustalic sobie dowolna przerwe. 

4) Powr�t do lokalnych zmian sprzed chwili/z przeszlosci:
<prawy myszy> -> Local History -> Show history
Kliknac na zmiane -> revert

5) Przy wrzucaniu zmian na git'a do pliku .gitignore wpisuje sie folder: .idea


Do sprawdzenia krok�w:
- pip --proxy nazwa:port install radish-bdd[testing]
- set PYTHONIOENCODING=utf-8
- chcp 65001
- radish-test --b radish matches step-matches.yml