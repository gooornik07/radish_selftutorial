@constant(quote: To be or not to be)
Feature: Text in step

  Scenario: Quote in steps
    Given I have the following quote
      """
      To be or not to be
      """
    When I add it to the results
    Then I expect ${quote} quote in the results