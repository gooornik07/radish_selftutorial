Feature:  Scenario-outline - "data driven"

  Scenario Outline: Multiplication
    Given I have the numbers <number1> and <number2>
    When I multiply them
    Then I expect the result to be <result>

    Examples:
    | number1 | number2 | result  |
    | 3       | 2       | 6       |
    | 7       | 8       | 56      |
    | 20      | 50      | 1000    |