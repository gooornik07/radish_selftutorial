Feature: Behave_like - call another steps within step

  Scenario: All operations
    Given I perform many operations on 10 and 5
    When I add 3 to the result
    Then I expect the result to be 6