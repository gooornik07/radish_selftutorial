@special_feature
Feature: Before and after is important

  Scenario: Check value from before feature
    Given I have the numbers 3 and 4
    When I sum them
    Then The result is not 7 because of before feature statement

  @special_tag
  Scenario: Check value from before scenario
    Given I have the numbers 3 and 4
    When I sum them
    Then The result is 5 because of before scenario statement

  @not_special_tag
  Scenario: Check value from before scenario2
    Given I have the numbers 3 and 4
    When I sum them
    Then The result is 5 because of before scenario statement

  @special_tag
  Scenario: World value
    Given I have the numbers 3 and 4
    When I sum them
    Then Check generated value different than 42