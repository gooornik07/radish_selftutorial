Feature: Precondition

  @precondition(single_scenario.feature: Numbers subtraction)
  Scenario: After precondition
    Given I have the numbers 5 and 2
    When I multiply them
    Then I expect the result to be 10