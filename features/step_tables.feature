Feature: Step tables - data in steps

  Scenario: Check database
    Given I have the following users
      | Andrew  | Baggaley  | Pingpong  |
      | Adam    | Malysz    | Malamysz  |
      | Thierry | Henry     | Legend    |
    When I add them to the database
    Then I expect 3 users in the database with 3 fields each