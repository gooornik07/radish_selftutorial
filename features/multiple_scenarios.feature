@regression
Feature: Multiple scenarios
	In order to test my awesome software
	I need an awesome BDD tool like radish
	to test my software

	@good_case
	Scenario:	Test my calculator
		Given I have the numbers 5 and 6
		When I sum them
		Then I expect the result to be 11

	@good_case
	Scenario:	Test his calculator
		Given I have the numbers 10 and 20
		When I sum them
		Then I expect the result to be 35

	@bad_case
	Scenario:	Test their calculator
		Given I have the numbers 100 and 1000
		When I sum them
		Then I expect the result to be 1100