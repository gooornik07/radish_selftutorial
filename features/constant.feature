@constant(base_time: 3)
Feature: Constant test

  Scenario: Check microwave time
    Given Microwave set to drift for ${base_time} minutes
    When Microwave time increased by 2 minutes
    Then Microwave is expected to drift for 5 minutes