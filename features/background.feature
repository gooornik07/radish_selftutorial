Feature: Background is run after the before hooks of each scenario but before the steps

  Background: Numbers 10 and 5
    Given I have the numbers 10 and 5

  Scenario: Multiply after background
    When I multiply them
    Then I expect the result to be 50

  Scenario: Sum after background
    When I sum them
    Then I expect the result to be 15