Feature: Class implementation

  Scenario: Name example
    Given I have the name Luki and numbers 3 and 2 and 1
    When I connect name and surname Gornik and sum list of numbers
    Then I expect the full name to be Luki Gornik and sum to be 6
