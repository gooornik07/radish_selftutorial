# -*- coding: utf-8 -*-

from radish import given, when, then


@when("I subtract them")
def subtract_numbers(step):
    step.context.result = step.context.number1 - step.context.number2