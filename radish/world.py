from radish import world, pick
import random

world.x = 42


@pick
def get_magic_number():
    return random.randint(1, world.x)