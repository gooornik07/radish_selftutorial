# -*- coding: utf-8 -*-

from radish import given, when, then


@when("I divide them")
def divide_numbers(step):
    step.context.result = step.context.number1/step.context.number2