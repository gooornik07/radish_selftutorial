# -*- coding: utf-8 -*-

from radish import step
from radish import given, when, then


@step("We sum numbers")
def sum_numbers_2(step):
    step.context.result = step.context.number1 + step.context.number2


@step("We divide result and number2")
def divide_numbers(step):
    step.context.result = step.context.result/step.context.number2


@step("I perform many operations on {number1:g} and {number2:g}")
def perform_all_operations(step, number1, number2):
    step.context.number1 = number1
    step.context.number2 = number2
    step.behave_like("We sum numbers")
    step.behave_like("We divide result and number2")


@when("I add {number:g} to the result")
def add_example_number(step, number):
    step.context.result += number


