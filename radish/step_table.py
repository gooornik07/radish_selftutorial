# -*- coding: utf-8 -*-

from radish import given, when, then

database = []


@given("I have the following users")
def have_users(step):
    step.context.users = step.table


@when("I add them to the database")
def add_to_database(step):
    step.context.database = database
    for user in step.context.users:
        temp_list = [user[0], user[1], user[2]]
        step.context.database.append(temp_list)


@then("I expect {number:g} users in the database with {count:g} fields each")
def expect_result(step, number, count):
    assert len(step.context.database) == number, "Database: " + str(step.context.database)
    for i in range(0, 2):
        assert len(step.context.database[i]) == count, "Expected length of data failed"
