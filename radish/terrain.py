from radish import before, after
from radish import world


@before.each_feature(on_tags = 'special_feature')
def setup(feature):
    feature.context.setup = True


@before.each_scenario(on_tags = 'special_tag')
def before_method(scenario):
    scenario.context.before_value = 5
    scenario.context.token = world.get_magic_number()


@after.each_scenario(on_tags = 'special_tag')
def after_method(scenario):
    scenario.context.before_value = 7



