#-*- coding: utf-8 -*-

from radish import given, when, then
from radish import terrain


@given("Microwave set to drift for {minutes:g} minutes")
def microwave_time_drift(step, minutes):
    step.context.minutes = minutes


@when("Microwave time increased by {minutes:g} minutes")
def microwave_time_inreased(step, minutes):
    step.context.minutes += minutes


@then("Microwave is expected to drift for {minutes:g} minutes")
def microwave_expected_time(step, minutes):
    assert step.context.minutes == minutes, "Time for microwave is not correct"