# -*- coding: utf-8 -*-

from radish import steps
from radish import custom_type, register_custom_type, TypeBuilder


@custom_type('Number', r'\d+')
def parse_number(text):
    return int(text)


@custom_type('FullName', r'[A-Z][a-z]+ [A-Z][a-z]+')
def name_with_surname(text):
    return str(text)


#register the NumberList type
register_custom_type(
    NumberList = TypeBuilder.with_many(
        parse_number, listsep='and'
    )
)

#register the NameAndSurname type
register_custom_type(
    NameAndSurname = TypeBuilder.with_zero_or_one(
        name_with_surname
    )
)


@steps
class ExampleClass(object):

    ignore = ["no_step"]

    def no_step(self, step, name):
        """No step {name:w}"""
        step.context.name = name + "false"

    def have_name(self, step, name, numbers):
        """I have the name {name:w} and numbers {numbers:NumberList}"""
        step.context.name = name
        step.context.numbers = numbers

    def connect_names(self, step, surname):
        """I connect name and surname {surname:w} and sum list of numbers"""
        step.context.full_name = step.context.name + " " + surname
        step.context.sum_numbers = sum(step.context.numbers)

    def expect_full_name(self, step, full_name, sum_numbers):
        """I expect the full name to be {full_name:NameAndSurname} and sum to be {sum_numbers:g}"""
        assert step.context.full_name == full_name, "Full name is not ok: " + step.context.full_name + " != " + full_name
        assert step.context.sum_numbers == sum_numbers, "Sum is not ok: " + str(step.context.sum_numbers) + " != " + str(sum_numbers)