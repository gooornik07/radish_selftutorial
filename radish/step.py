from radish.stepregistry import step
from radish import given, when, then


@step("We have the number {number:g}")
def have_number(step, number):
    step.context.numbers.append(number)


@when("We sum them")
def sum_numbers(step):
    step.context.result = sum(step.context.numbers)


@then("We expect the result to be {result:g}")
def expect_result(step, result):
    assert step.context.result == result