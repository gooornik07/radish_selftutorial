# -*- coding: utf-8 -*-

from radish import given, when, then


@when("I multiply them")
def multiply_numbers(step):
    step.context.result = step.context.number1 * step.context.number2