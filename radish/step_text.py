# -*- coding: utf-8 -*-

from radish import given, when, then


@given("I have the following quote")
def have_quote(step):
    step.context.quote = step.text


@when("I add it to the results")
def add_quote_to_results(step):
    step.context.results = []
    step.context.results.append(step.context.quote)


@then("I expect {quote:D} quote in the results")
def expect_amount_of_quotes(step, quote):
    assert len(step.context.results) == 1
    assert step.context.results[0] == quote, "Quote: {0} is not step.context.results[0]: {1}".format(quote, step.context.results)