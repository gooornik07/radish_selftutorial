# -*- coding: utf-8 -*-

from radish import given, when, then


@then("The result is {number:g} because of before scenario statement")
def false_result_scenario(step, number):
    assert step.context.before_value == number,\
        "The result is {0} because of before SCENARIO statement with actual value {1}".format(number, step.context.before_value)


@then("The result is not {number:g} because of before feature statement")
def false_result_feature(feature, number):
    assert feature.context.setup == True, \
        "The result is not {0} because of before FEATURE statement with actual value {1}".format(number, feature.context.setup)


@then("Check generated value different than {default_value:g}")
def check_generated(scenario, default_value):
    assert scenario.context.token == default_value, "New generated value is: {0} - different than default: {1}"\
            .format(scenario.context.token, default_value)
